package com.swaggercustompoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggercustompocApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwaggercustompocApplication.class, args);
	}
}
