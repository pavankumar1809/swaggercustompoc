package com.swaggercustompoc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(value = "MessageController", description = "Operations pertaining to messages")
public class MessageController {

	@ApiOperation(value = "Morning wish", notes = "you can get your morning message")
	@RequestMapping(value = "/morning", method = RequestMethod.GET)
	public String getMorningMsg() {
		return "Hello good morning world";
	}

	@ApiOperation(value = "Afternoon wish", notes = "you can get your afternoon message")
	@RequestMapping(value = "/afternoon", method = RequestMethod.GET)
	public String getAfternoonMsg() {
		return "hello good afternoon world";
	}

	@ApiOperation(value = "Evening wish", notes = "you can get your evening message")
	@RequestMapping(value = "/evening", method = RequestMethod.GET)
	public String getEveningMsg() {
		return "hello good evening world";
	}
}
