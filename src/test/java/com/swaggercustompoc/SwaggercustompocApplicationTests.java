package com.swaggercustompoc;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.github.swagger2markup.Swagger2MarkupConverter;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SwaggercustompocApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void convert() {
        //change the loction of files accordingly
		Path localSwaggerFile = Paths.get("D:/swaggercustompoc/target/generated-docs/swagger/swagger.yaml");
		Path outputDirectory = Paths.get("D:/swaggercustompoc/target/generated-docs/asciidoc");
		Swagger2MarkupConverter.from(localSwaggerFile).build().toFolder(outputDirectory);
	}

}
