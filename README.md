Integration of Spring boot with Swagger.

Change the locations of files in the  /test/java/com/swaggercustompoc/SwaggercustompocApplicationTests.java

To run the project 

mvn spring-boot:run

To generate the documentation in pdf and html, run following commands.

mvn clean

mvn compile

mvn package
